<?php
class CornLog{
    public static function create_tet_corn_log_table(){
        global $wpdb;

        $table_name = $wpdb->prefix . "tet_corn_log";
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  created timestamp NOT NULL default CURRENT_TIMESTAMP,
		  product_updated INT NOT NULL,
		  product_added INT NOT NULL,
		  product_skip INT NOT NULL,
		  product_error INT NOT NULL,
		  add_type VARCHAR(50) NOT NULL,
		  UNIQUE KEY id (id)
		) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        add_option( 'tet_db_version', theetree_db_version );
    }



    public static function tet_add_to_log($product_updated,$product_added,$product_skip,$product_error,$add_type){
        global $wpdb;

        $table_name = $wpdb->prefix . "tet_corn_log";

        $data = array('product_updated' => $product_updated, 'product_added' => $product_added,
            'product_skip'=> $product_skip, 'product_error'=>$product_error,'add_type'=>$add_type);
        $format = array('%s','%d');

        $wpdb->insert($table_name,$data,$format);
    }

    public static function tet_get_all_log(){
        global $wpdb;

        $table_name = $wpdb->prefix . "tet_corn_log";

        return $wpdb->get_results("SELECT * FROM ".$table_name,ARRAY_A);
    }
}

CornLog::create_tet_corn_log_table();
