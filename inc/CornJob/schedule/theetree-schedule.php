<?php

class TheeTreeSchedules{
    public static $theetree_supported_times = [
        'theetree_6_hour' => 6*60*60,
        'theetree_12_hour'=>12*60*60,
        'theetree_daily' => 24*60*60
    ];

    public static function clear_theetree_schedules(){
        wp_clear_scheduled_hook('callback_scheduled_theetree');
    }

    public static function start_cron_job(){
        $time_selected = TheETreeHelper::get_selected_time();

        if(is_null($time_selected)){
            $time_selected = 'theetree_daily';
        }
        wp_schedule_event(strtotime('01:00:00'), $time_selected, 'callback_scheduled_theetree');

    }

    public static function get_next_time(){
        $time = wp_next_scheduled('callback_scheduled_theetree');
        if(is_null($time)){
            return '';
        }
        return get_date_from_gmt( date('Y-m-d H:i:s', $time) );
    }
}
$s6h = __('theetree_6_hour','theetree-woocommerce-dropshipping');
$s12h = __('theetree_12_hour','theetree-woocommerce-dropshipping');
$sdaily = __('theetree_daily','theetree-woocommerce-dropshipping');

function theetree_schedules($schedules){

    foreach (TheeTreeSchedules::$theetree_supported_times as $index => $time){
        if(!isset($schedules[$index])){
            $schedules[$index] = array(
                'interval' => $time,
                'display' => __($time,'theetree'));
        }
    }

    return $schedules;
}
add_filter('cron_schedules','theetree_schedules');



add_action( 'callback_scheduled_theetree', 'callback_scheduled_theetree' );

function callback_scheduled_theetree(){

    CornLog::tet_add_to_log(0,0,0,0,'Start Cron');

    $products_status = ImportHelper::sync_products();

    CornLog::tet_add_to_log(count($products_status['updated']), count($products_status['imported']),
       count($products_status['skipped']),count($products_status['failed']),'Cron');


    return 'Updated';

}