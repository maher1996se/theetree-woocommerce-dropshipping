<?php


if (!class_exists('TheETreeHelper')) {
    class TheETreeHelper
    {

        public static function theetree_get_options($key = null)
        {
            $options = get_option(THEETREE_OPTIONS_KEYS, array());
            if (!is_null($key)) {
                $options = isset($options[$key]) ? $options[$key] : '';
            }
            return $options;
        }


        public static function theetree_update_options($options)
        {
            update_option(THEETREE_OPTIONS_KEYS, $options);
        }

        public static function get_consumer_key(){
            return TheETreeHelper::theetree_get_options('consumer_key');
        }

        public static function get_selected_time(){
            return TheETreeHelper::theetree_get_options('update_rate');
        }

        public static function get_consumer_secret(){
            return TheETreeHelper::theetree_get_options('consumer_secret');
        }

        public static function set_access_token($access_token){
            $options = TheETreeHelper::theetree_get_options();

            $options['access_token'] = $access_token;

            self::theetree_update_options($options);
        }

        public static function get_access_token(){
            return TheETreeHelper::theetree_get_options('access_token');

        }

       public static function save_csv_file( $name, $deprecated, $bits, $time = null ) {
            if ( ! empty( $deprecated ) ) {
                _deprecated_argument( __FUNCTION__, '2.0.0' );
            }

            if ( empty( $name ) ) {
                return array( 'error' => __( 'Empty filename' ) );
            }

            $wp_filetype = wp_check_filetype( $name );
//            if ( ! $wp_filetype['ext'] && ! current_user_can( 'unfiltered_upload' ) ) {
//                return array( 'error' => __( 'Sorry, this file type is not permitted for security reasons.' ) );
//            }

            $upload = wp_upload_dir( $time );

            if ( false !== $upload['error'] ) {
                return $upload;
            }

            /**
             * Filters whether to treat the upload bits as an error.
             *
             * Returning a non-array from the filter will effectively short-circuit preparing the upload bits
             * and return that value instead. An error message should be returned as a string.
             *
             * @since 3.0.0
             *
             * @param array|string $upload_bits_error An array of upload bits data, or error message to return.
             */
            $upload_bits_error = apply_filters(
                'wp_upload_bits',
                array(
                    'name' => $name,
                    'bits' => $bits,
                    'time' => $time,
                )
            );
            if ( ! is_array( $upload_bits_error ) ) {
                $upload['error'] = $upload_bits_error;
                return $upload;
            }

            $filename = wp_unique_filename( $upload['path'], $name );

            $new_file = $upload['path'] . "/$filename";
            if ( ! wp_mkdir_p( dirname( $new_file ) ) ) {
                if ( 0 === strpos( $upload['basedir'], ABSPATH ) ) {
                    $error_path = str_replace( ABSPATH, '', $upload['basedir'] ) . $upload['subdir'];
                } else {
                    $error_path = wp_basename( $upload['basedir'] ) . $upload['subdir'];
                }

                $message = sprintf(
                /* translators: %s: Directory path. */
                    __( 'Unable to create directory %s. Is its parent directory writable by the server?' ),
                    $error_path
                );
                return array( 'error' => $message );
            }

            $ifp = @fopen( $new_file, 'wb' );
            if ( ! $ifp ) {
                return array(
                    /* translators: %s: File name. */
                    'error' => sprintf( __( 'Could not write file %s' ), $new_file ),
                );
            }

            fwrite( $ifp, $bits );
            fclose( $ifp );
            clearstatcache();

            // Set correct file permissions.
            $stat  = @ stat( dirname( $new_file ) );
            $perms = $stat['mode'] & 0007777;
            $perms = $perms & 0000666;
            chmod( $new_file, $perms );
            clearstatcache();

            // Compute the URL.
            $url = $upload['url'] . "/$filename";

            /** This filter is documented in wp-admin/includes/file.php */
            return apply_filters(
                'wp_handle_upload',
                array(
                    'file'  => $new_file,
                    'url'   => $url,
                    'type'  => $wp_filetype['type'],
                    'error' => false,
                ),
                'sideload'
            );
        }
    }
}

