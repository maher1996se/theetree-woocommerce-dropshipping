<?php
///**
// * TGMPA Required Plugins.
// *
// * Register the required plugins for this plugin.
// *
// */
//
//function theetree_register_required_plugins() {
//	$plugins = array(
//
//		array(
//			'name'         	=> esc_html__( 'WooCommerce', 'dropshipping-woocommerce' ),
//			'slug'          => 'woocommerce',
//			'required'     	=> true,
//			'recommended_by'=> 'TheETree'
//		)
//	);
//
//
//	$config = array(
//		'id'           => 'dropshipping-woocommerce', // Unique ID for hashing notices for multiple instances of TGMPA.
//		'default_path' => '',                      // Default absolute path to bundled plugins.
//		'menu'         => 'tgmpa-install-plugins', // Menu slug.
//		'parent_slug'  => 'plugins.php',            // Parent menu slug.
//		'has_notices'  => true,                    // Show admin notices or not.
//		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
//		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be at the top of nag.
//		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
//		'message'      => '',                      // Message to output right before the plugins table.
//
//		'strings'      => array(
//
//			'notice_can_install_required'     => _n_noop(
//				/* translators: 1: plugin name(s). */
//				'Dropshipping requires the following plugin: %1$s.',
//				'Dropshipping requires the following plugins: %1$s.',
//				'dropshipping-woocommerce'
//			),
//			'notice_can_install_recommended'  => _n_noop(
//				/* translators: 1: plugin name(s). */
//				'Dropshipping recommends the following plugin: %1$s.',
//				'Dropshipping recommends the following plugins: %1$s.',
//				'dropshipping-woocommerce'
//			)
//		),
//	);
//
//	tgmpa( $plugins, $config );
//}
//
//add_action( 'tgmpa_register', 'theetree_register_required_plugins' );
