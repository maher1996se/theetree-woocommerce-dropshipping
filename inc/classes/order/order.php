<?php


// Add your custom order status action button (for orders with "processing" status)
add_filter('woocommerce_admin_order_actions', 'add_custom_order_status_actions_button', 100, 2);
function add_custom_order_status_actions_button($actions, $order)
{
    // Display the button for all orders that have a 'processing' status
    if ($order->has_status(array('processing'))) {

        // Get Order ID (compatibility all WC versions)
        $order_id = method_exists($order, 'get_id') ? $order->get_id() : $order->id;
        // Set the action button
        $actions['addorderetree'] = array(
            'url' => wp_nonce_url(admin_url('admin.php?action=tet_add_new_order&order_id=' . $order_id), 'woocommerce-mark-order-status'),
            'name' => __('Submit TheETree Order', 'theetree-woocommerce-dropshipping'),
            'action' => "view addorderetree",
        );
    }
    return $actions;
}

// Set Here the WooCommerce icon for your action button
add_action('admin_head', 'add_custom_order_status_actions_button_css');
function add_custom_order_status_actions_button_css()
{
    echo '<style>.view.addorderetree::after { content: "\f242" !important; }</style>';
}


function create_tet_order_table()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tet_order";
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  created timestamp NOT NULL default CURRENT_TIMESTAMP,
		  wc_order_id varchar(100) NOT NULL,
		  tet_order_id varchar(100) NOT NULL,
		  tet_order_uuid varchar(100) NOT NULL,
		  UNIQUE KEY id (id)
		) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    add_option('tet_db_version', theetree_db_version);
}

create_tet_order_table();

function check_if_order_exist($wc_order_id)
{
    return !is_null(get_tet_order_id($wc_order_id));
}

function get_tet_order_id($wc_order_id)
{
    global $wpdb;

    $row = $wpdb->get_row("SELECT tet_order_id FROM " . $wpdb->prefix . "tet_order WHERE wc_order_id = " . $wc_order_id . " LIMIT 1");

    $data = is_null($row) ? null : $row->tet_order_id;

    return $data;
}

function add_tet_order_to_db_table($wc_order_id,$tet_order_id,$tet_order_uuid){
    global $wpdb;
    $table_name = $wpdb->prefix . "tet_order";

    $wpdb->insert($table_name, array('wc_order_id' => $wc_order_id,
        'tet_order_id' => $tet_order_id,'tet_order_uuid'=>$tet_order_uuid) );
    return true;
}