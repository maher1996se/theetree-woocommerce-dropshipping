<?php


class OrderHelper
{
   public static function tet_add_order(OrderObject $order,$wco_id)
    {
        $order_json = json_encode($order);
        $response = TheETreeApis::addOrder($order_json);

        if(is_null($response)){
            return null;
        }

        if ($response['response_status'] == 200){
            add_tet_order_to_db_table($wco_id,$response['data']['id'],$response['data']['uuid']);

            return array('is_done'=>true,'message'=>'Order Added',
                'tet_order_id'=>$response['data']['id'],'tet_order_uuid'=>$response['data']['uuid']);
        }

        if ($response['response_status'] == 520){
            return array('is_done'=>false,'message'=>$response['message'],
                'tet_order_id'=>null,'tet_order_uuid'=>null);
        }

        return null;
    }
}