<?php
/**
 * Plugin Name:       TheETree WooCommerce DropShipping
 * Version:           1.0.0
 * Author:            TheETree
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       theetree-woocommerce-dropshipping
 * Domain Path:       languages
 * WC requires at least: 3.3.0
 * WC tested up to: 4.2.2
 *
 * @package     TheETree_Dropshipping
 */

if (!defined('ABSPATH')) exit;


if( ! defined( 'THEETREE_PLUGIN_DIR' ) ){
    define( 'THEETREE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

if( ! defined( 'THEETREE_PLUGIN_URL' ) ){
    define( 'THEETREE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

if( ! defined( 'THEETREE_PLUGIN_FILE' ) ){
    define( 'THEETREE_PLUGIN_FILE', __FILE__ );
}

if( ! defined( 'THEETREE_OPTIONS_KEYS' ) ){
    define( 'THEETREE_OPTIONS_KEYS', 'theetree_options_keys' );
}


if( ! defined( 'THEETREE_DEBUG_IN_URL' ) ){
    define( 'THEETREE_DEBUG_IN_URL', false );
}

if( ! defined( 'THEETREE_SERVER_API_URL' ) ){
   // define( 'THEETREE_SERVER_API_URL', 'http://192.168.1.33:8000/api/' );
    define( 'THEETREE_SERVER_API_URL', 'http://127.0.0.1:8000/api/' );
    //define( 'THEETREE_SERVER_API_URL', 'https://app.theetree.com/api/' );
    //define( 'THEETREE_SERVER_API_URL', 'https://dev.theetree.com/api/' );

}

$ds = DIRECTORY_SEPARATOR;
$base_dir = realpath(dirname(__FILE__)  . $ds . '..') . $ds;
$file = "{$base_dir}woocommerce{$ds}woocommerce.php";
include_once($file);

 define( 'theetree_db_version', '1.2' );

require_once 'inc/classes/required-plugins.php';
require_once 'inc/classes/apis/TheETreeApis.php';
require_once 'inc/classes/helpers/TheETreeHelper.php';
require_once 'inc/classes/helpers/TETNotices.php';
require_once 'inc/CornJob/LogPage.php';
require_once 'inc/CornJob/CornLog.php';
require_once 'inc/CornJob/schedule/theetree-schedule.php';
require_once 'inc/classes/admin/TheETree_Admin.php';
require_once 'inc/classes/importer/TheETreeImporterToWC.php';
require_once 'inc/classes/importer/importer.php';
require_once 'inc/classes/importer/ImportCsvByFile.php';
require_once 'inc/classes/importer/ImportHelper.php';
require_once 'inc/actions/products.php';
require_once 'inc/actions/order.php';
require_once 'inc/classes/order/order.php';
require_once 'inc/classes/order/OrderHelper.php';
require_once 'inc/classes/order/OrderObject.php';
require_once 'inc/classes/order/OrderStatus.php';




if (!class_exists('TheETree_DropShipping')) {
    class TheETree_DropShipping
    {
        private static $admin;


        public function __construct()
        {
            self::$admin = new TheETree_Admin();

        }


    }

}

$theetree = new TheETree_DropShipping();


//* Register activation and deactivation hooks
register_activation_hook( __FILE__ , 'theetree_activation' );
register_deactivation_hook( __FILE__ , 'theetree_deactivation' );

//* Add upload_csv capability to administrator role
function theetree_activation() {
    $admin = get_role( 'administrator' );
    $admin->add_cap( 'upload_csv' );
}

//* Remove upload_csv capability from administrator role
function theetree_deactivation() {
    $admin = get_role( 'administrator' );
    $admin->remove_cap( 'upload_csv' );
}

//* Add filter to check filetype and extension
add_filter( 'wp_check_filetype_and_ext', 'theetree_check_filetype_and_ext', 10, 4 );

//* If the current user can upload_csv and the file extension is csv, override arguments - edit - "$pathinfo" changed to "pathinfo"
function theetree_check_filetype_and_ext( $args, $file, $filename, $mimes ) {
    if( current_user_can( 'upload_csv' ) && 'csv' === pathinfo( $filename )[ 'extension' ] ){
        $args = array(
            'ext'             => 'csv',
            'type'            => 'text/csv',
            'proper_filename' => $filename,
        );
    }
    return $args;
}

// Display admin product custom setting field(s)
add_action('woocommerce_product_options_general_product_data', 'theetree_woocommerce_product_custom_fields');
function theetree_woocommerce_product_custom_fields() {
    global $product_object;

    echo '<div class=" product_custom_field ">';

    // Custom Product Text Field
    woocommerce_wp_text_input( array(
        'id'          => 'the_e_tree_metadata',
        'label'       => __('The E Tree Metadata', 'woocommerce'),
        'placeholder' => '',
        'desc_tip'    => 'true' // <== Not needed as you don't use a description
    ) );

    echo '</div>';
}

// Save admin product custom setting field(s) values
add_action('woocommerce_admin_process_product_object', 'theetree_woocommerce_product_custom_fields_save');
function theetree_woocommerce_product_custom_fields_save( $product ) {
    if ( isset($_POST['the_e_tree_metadata']) ) {
        $product->update_meta_data( 'the_e_tree_metadata', sanitize_text_field( $_POST['the_e_tree_metadata'] ) );
    }
}


function theetree_plugin_load_textdomain() {
  load_plugin_textdomain( 'theetree-woocommerce-dropshipping', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

}
add_action( 'plugins_loaded', 'theetree_plugin_load_textdomain' );